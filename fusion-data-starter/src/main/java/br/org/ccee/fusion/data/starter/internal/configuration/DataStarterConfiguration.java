package br.org.ccee.fusion.data.starter.internal.configuration;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import br.org.ccee.fusion.data.jpa.internal.configuration.DataJpaConfiguration;

@Configuration
@Import({ DataJpaConfiguration.class })
@AutoConfigureAfter({ TransactionAutoConfiguration.class })
public class DataStarterConfiguration {

}
