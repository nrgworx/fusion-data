package br.org.ccee.fusion.data.jpa.internal.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.commons.core.api.exception.NotFoundException;
import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.commons.core.api.model.ObjectGraphNode;
import br.org.ccee.fusion.data.sample.api.model.City;
import br.org.ccee.fusion.data.sample.api.model.Hotel;
import br.org.ccee.fusion.data.sample.api.model.QHotel;
import br.org.ccee.fusion.data.sample.api.model.QReview;
import br.org.ccee.fusion.data.sample.api.model.Review;
import br.org.ccee.fusion.data.sample.api.repository.CityRepository;
import br.org.ccee.fusion.data.sample.api.repository.ExtendedCityRepository;
import br.org.ccee.fusion.data.sample.api.repository.HotelRepository;
import br.org.ccee.fusion.data.sample.api.repository.ReviewRepository;
import br.org.ccee.fusion.data.sample.internal.configuration.SampleDataJpaConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SampleDataJpaConfiguration.class)
@DataJpaTest()
public class EntityTenancyFilter {

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private ExtendedCityRepository extendedCityRepository;

	@Autowired
	private HotelRepository hotelRepository;

	@Autowired
	private ReviewRepository reviewRepository;

	@Test
	public void testGetEntityFilter00() {
		Page<City> page = this.cityRepository.findAll(PageRequest.of(0, 100), Optional.empty(), Optional.empty());
		assertNotNull(page);
		assertEquals(20, page.getContent().size());
	}

	@Test
	public void testGetEntityFilter01() {
		City city = new City();
		city.setId(18L);
		city.setCountry("USA");
		city.setName("New York");
		city.setState("NY");
		city.setMap("0, 0");
		try {
			this.cityRepository.save(city);
			this.cityRepository.flush();
		} catch (NotFoundException notFoundException) {
			assertNotNull(notFoundException);
			System.out.println(notFoundException);
		}
	}

	@Test
	public void testGetEntityFilter02() {
		// 24 -> 70 Park Avenue Hotel em New York, nao retorna pois esta composto por
		// New York
		Optional<Hotel> hotel = this.hotelRepository.findById(24L,
				Optional.empty());
		assertFalse(hotel.isPresent());
	}

	@Test
	public void testGetEntityFilter03() {
		// 24 -> 70 Park Avenue Hotel em New York, não retorna nada porque não posso ver
		// New York
		ObjectGraphNode objectGraphNode = new ObjectGraphNode("city");
		List<ObjectGraphNode> objectGraphNodes = new ArrayList<>();
		objectGraphNodes.add(objectGraphNode);
		ObjectGraph objectGraph = new ObjectGraph(objectGraphNodes);
		Optional<Hotel> hotel = this.hotelRepository.findById(24L,
				Optional.of(objectGraph));
		assertFalse(hotel.isPresent());
	}

	@Test
	public void testGetEntityFilter04() {
		ObjectGraphNode objectGraphNode = new ObjectGraphNode("reviews");
		List<ObjectGraphNode> objectGraphNodes = new ArrayList<>();
		objectGraphNodes.add(objectGraphNode);
		ObjectGraph objectGraph = new ObjectGraph(objectGraphNodes);
		Hotel hotel = this.hotelRepository.findById(27L, Optional.of(objectGraph))
				.get();
		assertNotNull(hotel);
		assertEquals(1, hotel.getReviews().size());
	}

	@Test
	public void testGetEntityFilter05() {
		// Retorna apenar uma poque não podemos ver reviews com a palavra "Avoid"
		Predicate predicate = QReview.review.hotel.id.eq(27L);
		List<Review> reviews = this.reviewRepository.findAll(Optional.of(predicate), Optional.empty()).collect(Collectors.toList());
		assertNotNull(reviews);
		assertEquals(1, reviews.size());
	}

	@Test
	public void testGetEntityFilter07() {
		Hotel hotel = this.hotelRepository.findById(27L, Optional.empty()).get();
		Set<Review> reviews = hotel.getReviews();
		assertNotNull(reviews);
		assertEquals(2, reviews.size());
	}

	@Test
	public void testGetEntityFilter08() {
		Predicate predicate = QHotel.hotel.reviews.any().hotel.id.eq(24L);
		List<Hotel> hotels = this.hotelRepository.findAll(Optional.of(predicate), Optional.empty()).collect(Collectors.toList());
		assertNotNull(hotels);
		assertEquals(0, hotels.size());
	}

	@Test
	public void testGetEntityFilter09() {
		ObjectGraphNode objectGraphNode = new ObjectGraphNode("hotels");
		objectGraphNode.addChild(new ObjectGraphNode("reviews"));
		List<ObjectGraphNode> objectGraphNodes = new ArrayList<>();
		objectGraphNodes.add(objectGraphNode);
		ObjectGraph objectGraph = new ObjectGraph(objectGraphNodes);
		City city = this.cityRepository.findById(21L, Optional.of(objectGraph))
				.get();
		assertNotNull(city);
		assertEquals(1, city.getHotels().size());
		assertEquals(1, city.getHotels().iterator().next().getReviews().size());
	}

	@Test
	public void testGetEntityFilter10() {
		List<City> cities = this.extendedCityRepository.findByName("Washington");
		assertNotNull(cities);
		assertEquals(1, cities.size());
		assertEquals("Washington", cities.get(0).getName());
	}

	@Test
	public void testGetEntityFilter11() {
		List<City> cities = this.extendedCityRepository.findByCountry("USA");
		assertNotNull(cities);
		assertEquals(10, cities.size());
	}

	@Ignore
	@Test
	public void testGetEntityFilter12() {
		City city = this.extendedCityRepository.findFirstByCountry("USA");
		assertNotNull(city);
	}

	@Test
	public void testGetEntityFilter13() {
		Hotel hotel = this.hotelRepository.findById(27l, Optional.empty()).get();
		Set<Review> reviews = hotel.getReviews();
		assertNotNull(reviews);
		assertEquals(1, reviews.size());
	}

	@Test
	public void testGetEntityFilter14() {
		Review review = this.reviewRepository.findById(62l, Optional.empty()).get();
		assertNotNull(review);
	}

}
