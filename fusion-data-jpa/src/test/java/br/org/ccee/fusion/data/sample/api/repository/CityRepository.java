package br.org.ccee.fusion.data.sample.api.repository;

import br.org.ccee.fusion.data.core.api.repository.PersistableRepository;
import br.org.ccee.fusion.data.sample.api.model.City;

public interface CityRepository extends PersistableRepository<City, Long> {

}
