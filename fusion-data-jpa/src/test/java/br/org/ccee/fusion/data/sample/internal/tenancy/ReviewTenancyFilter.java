package br.org.ccee.fusion.data.sample.internal.tenancy;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.commons.core.api.model.Operation;
import br.org.ccee.fusion.data.jpa.api.component.EntityFilter;
import br.org.ccee.fusion.data.sample.api.model.QReview;
import br.org.ccee.fusion.data.sample.api.model.Review;

@Component
public class ReviewTenancyFilter implements EntityFilter<Review> {

	@Override
	public Optional<Predicate> getPredicate(Operation operation) {
		return Optional.of(QReview.review.title.notEqualsIgnoreCase("Avoid"));
	}

}
