package br.org.ccee.fusion.data.jpa.internal.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigInteger;
import java.util.LinkedHashSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.org.ccee.fusion.commons.core.api.exception.NotAuthorizedException;
import br.org.ccee.fusion.data.sample.api.model.City;
import br.org.ccee.fusion.data.sample.api.model.Hotel;
import br.org.ccee.fusion.data.sample.api.repository.CityRepository;
import br.org.ccee.fusion.data.sample.api.repository.HotelRepository;
import br.org.ccee.fusion.data.sample.internal.configuration.SampleDataJpaConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SampleDataJpaConfiguration.class)
@DataJpaTest()
public class EntityLoaderTest {

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private HotelRepository hotelRepository;

	// Preservo the hotels list with orphanRemoval=true
	@Test
	public void deepUpdateTest01() throws NotAuthorizedException {
		City city = new City();
		city.setId(2L);
		city.setCountry("Australia");
		city.setName("Melbourne");
		city.setState("Victoria");
		city.setMap("0, 0");
		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		assertEquals("0, 0", city.getMap());
		assertEquals(1, city.getHotels().size());
	}

	@Test
	public void deepUpdateTest02() throws NotAuthorizedException {
		City city = new City();
		city.setId(2L);
		city.setCountry("Australia");
		city.setName("Melbourne");
		city.setState("Victoria");
		city.setMap("0, 0");
		city.setHotels(new LinkedHashSet<Hotel>());
		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		assertEquals(0, city.getHotels().size());
	}

	@Test
	public void deepUpdateTest03() throws NotAuthorizedException {

		City city = new City();
		city.setId(9L);
		city.setCountry("UK");
		city.setName("Bath");
		city.setState("Somerset");
		city.setMap("0, 0");
		city.setHotels(new LinkedHashSet<Hotel>());

		Hotel hotel = new Hotel();
		hotel.setId(9L);
		hotel.setName("New Bath Priory Hotel");
		hotel.setAddress("Weston Road");
		hotel.setZip("BA1 2XT");
		city.getHotels().add(hotel);

		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		assertEquals(1, city.getHotels().size());
	}

	@Test
	public void deepUpdateTest04() throws NotAuthorizedException {

		City city = new City();
		city.setId(9L);
		// city.setCountry("UK");
		city.setName("Assis");
		// city.setState("Somerset");
		// city.setMap("0, 0");
		/*
		 * city.setHotels(new LinkedHashSet<Hotel>());
		 * 
		 * Hotel hotel = new Hotel(); hotel.setId(BigInteger.valueOf(9L));
		 * hotel.setName("The Bath Priory Hotel"); hotel.setAddress("Weston Road");
		 * hotel.setZip("BA1 2XT"); city.getHotels().add(hotel);
		 * 
		 * hotel = new Hotel(); hotel.setCity(city); hotel.setName("Assis Hotel");
		 * hotel.setAddress("Sapopemba Road"); hotel.setZip("03227-070");
		 * city.getHotels().add(hotel);
		 */

		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		assertEquals(2, city.getHotels().size());
	}

	@Test
	public void deepUpdateTest05() throws NotAuthorizedException {

		City city = new City();
		city.setId(9L);
		city.setCountry("UK");
		city.setName("Bath");
		city.setState("Somerset");
		city.setMap("0, 0");

		Hotel hotel = new Hotel();
		hotel.setCity(city);
		hotel.setName("Assis Hotel 2");
		hotel.setAddress("Sapopemba Road");
		hotel.setZip("03227-070");

		hotel = this.hotelRepository.save(hotel);
		this.cityRepository.flush();
		assertNotNull(hotel);
		assertNotNull(hotel.getId());
		assertEquals("Assis Hotel 2", hotel.getName());
	}

	@Test
	public void deepUpdateTest06() throws NotAuthorizedException {

		City city = new City();
		city.setId(9L);
		city.setCountry("UK");
		city.setName("Bath");
		city.setState("Somerset");
		city.setMap("0, 0");
		city.setHotels(new LinkedHashSet<Hotel>());

		Hotel hotel = new Hotel();
		hotel.setId(9L);
		hotel.setName("The Bath Priory Hotel");
		hotel.setAddress("Weston Road");
		hotel.setZip("BA1 2XT");
		city.getHotels().add(hotel);

		hotel = new Hotel();
		hotel.setCity(city);
		hotel.setName("Assis Hotel");
		hotel.setAddress("Sapopemba Road");
		hotel.setZip("03227-070");
		city.getHotels().add(hotel);

		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		assertEquals(2, city.getHotels().size());
	}

	@Test
	public void deepUpdateTest07() throws NotAuthorizedException {

		City city = new City();
		city.setCountry("UK");
		city.setName("Assis");
		city.setState("Somerset");
		city.setMap("0, 0");
		city.setHotels(new LinkedHashSet<Hotel>());

		Hotel hotel = new Hotel();
		hotel.setId(9L);
		hotel.setName("The Bath Priory Hotel (Assis)");
		hotel.setAddress("Weston Road");
		hotel.setZip("BA1 2XT");
		city.getHotels().add(hotel);

		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		assertEquals(1, city.getHotels().size());
	}

	@Test
	public void deepUpdateTest08() throws NotAuthorizedException {

		City city = new City();
		city.setId(9L);
		city.setCountry("UK");
		city.setName("Bath (NEW)");
		city.setState("Somerset");
		city.setMap("0, 0");

		Hotel hotel = new Hotel();
		hotel.setName("Marcelao Plaza Hotel");
		hotel.setAddress("Weston Road");
		hotel.setZip("BA1 2XT");
		hotel.setCity(city);

		hotel = this.hotelRepository.save(hotel);
		this.cityRepository.flush();
		assertNotNull(hotel);
		assertEquals(BigInteger.valueOf(9L), hotel.getCity().getId());
		// Não pode atualizar o hotel por conta do cascade
		assertEquals("Bath", hotel.getCity().getName());
	}

	@Test
	public void deepUpdateTest09() throws NotAuthorizedException {

		// path only name! uhuuuu
		City city = new City();
		city.setId(9L);
		city.setName("Baths");
		city.setHotels(new LinkedHashSet<Hotel>());

		Hotel hotel = new Hotel();
		hotel.setId(9L);
		hotel.setName("The Bath Priory Hotel");
		hotel.setAddress("Weston Road");
		hotel.setZip("BA1 2XT");
		city.getHotels().add(hotel);

		hotel = new Hotel();
		hotel.setCity(city);
		hotel.setName("Assis Hotel");
		hotel.setAddress("Sapopemba Road");
		hotel.setZip("03227-070");
		city.getHotels().add(hotel);

		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		assertEquals(2, city.getHotels().size());
	}
}
