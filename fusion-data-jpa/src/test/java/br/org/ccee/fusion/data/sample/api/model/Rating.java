package br.org.ccee.fusion.data.sample.api.model;

public enum Rating {

	TERRIBLE, POOR, AVERAGE, GOOD, EXCELLENT

}
