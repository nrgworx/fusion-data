package br.org.ccee.fusion.data.sample.internal.tenancy;

import org.springframework.stereotype.Component;

import br.org.ccee.fusion.data.jpa.api.component.EntityFilter;
import br.org.ccee.fusion.data.sample.api.model.Hotel;

@Component
public class HotelTenancyFilter implements EntityFilter<Hotel> {

}
