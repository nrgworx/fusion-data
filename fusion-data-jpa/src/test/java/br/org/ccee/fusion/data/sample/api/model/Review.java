package br.org.ccee.fusion.data.sample.api.model;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.util.Assert;

@Entity
@Audited
public class Review extends AbstractPersistable<Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne(optional = false, cascade = { CascadeType.REFRESH, CascadeType.DETACH })
	@JoinColumn(name = "hotel_id")
	private Hotel hotel;

	@Column(name = "idx", nullable = false)
	private int index;

	@Column(name = "rating", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private Rating rating;

	@Column(name = "check_in_date", nullable = false)
	private Instant checkInDate;

	@Column(name = "trip_type", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private TripType tripType;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "details", nullable = true)
	private String details;

	protected Review() {
	}

	public Review(Hotel hotel, int index, ReviewDetails details) {
		Assert.notNull(hotel, "Hotel must not be null");
		Assert.notNull(details, "Details must not be null");
		setHotel(hotel);
		setIndex(index);
		setRating(details.getRating());
		setCheckInDate(details.getCheckInDate());
		setTripType(details.getTripType());
		setTitle(details.getTitle());
		setDetails(details.getDetails());
	}

	@Override
	public void setId(Long id) {
		super.setId(id);
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

	public Instant getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Instant checkInDate) {
		this.checkInDate = checkInDate;
	}

	public TripType getTripType() {
		return tripType;
	}

	public void setTripType(TripType tripType) {
		this.tripType = tripType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
