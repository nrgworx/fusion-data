package br.org.ccee.fusion.data.sample.internal.repository;

import java.util.Optional;
import java.util.stream.Collectors;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.data.sample.api.model.City;
import br.org.ccee.fusion.data.sample.api.model.QCity;
import br.org.ccee.fusion.data.sample.api.repository.CityRepository;
import br.org.ccee.fusion.data.sample.api.repository.CityRepositoryCustom;

public class CustomCityRepository implements CityRepositoryCustom {

	private CityRepository cityRepository;

	public CustomCityRepository(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	};

	@Override
	public City findFirstByCountry(String country) {
		Predicate predicate = QCity.city.country.eq(country);
		return this.cityRepository.findAll(Optional.of(predicate), Optional.empty())
				.collect(Collectors.toList())
				.get(0);
	}

}
