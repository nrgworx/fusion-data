package br.org.ccee.fusion.data.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import br.org.ccee.fusion.data.jpa.internal.configuration.DataJpaConfiguration;

@SpringBootApplication
@EnableAutoConfiguration
@Import(DataJpaConfiguration.class)
@ComponentScan("br.org.ccee.fusion.data.sample")
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}

}
