package br.org.ccee.fusion.data.jpa.internal.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.data.sample.api.model.City;
import br.org.ccee.fusion.data.sample.api.model.QCity;
import br.org.ccee.fusion.data.sample.api.repository.CityRepository;
import br.org.ccee.fusion.data.sample.internal.configuration.SampleDataJpaConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SampleDataJpaConfiguration.class)
@DataJpaTest()
public class GenericRepositoryTest {

	@Autowired
	private CityRepository cityRepository;

	@Test
	public void testGetEntityFilter00() {
		Predicate predicate = QCity.city.name.eq("Melbourne");
		Page<City> page = this.cityRepository.findAll(PageRequest.of(0, 100), Optional.of(predicate), Optional.empty());
		assertNotNull(page);
		assertEquals(2, page.getContent().size());
	}

	@Test
	public void testGetEntityFilter01() {
		Predicate predicate = QCity.city.name.eq("Melbourne").and(QCity.city.state.eq("Victoria"));
		Page<City> page = this.cityRepository.findAll(PageRequest.of(0, 100), Optional.of(predicate), Optional.empty());
		assertNotNull(page);
		assertEquals(1, page.getContent().size());
	}

	@Test
	public void testGetEntityFilter02() {
		Predicate predicate = QCity.city.name.eq("Melbourne").or(QCity.city.state.eq("Queensland"));
		Page<City> page = this.cityRepository.findAll(PageRequest.of(0, 100), Optional.of(predicate), Optional.empty());
		assertNotNull(page);
		assertEquals(3, page.getContent().size());
	}

	@Test
	public void testGetEntityFilter03() {
		Predicate predicate = ExpressionUtils.and(QCity.city.name.eq("Melbourne").or(QCity.city.state.eq("Queensland")), QCity.city.id.eq(1L));
		Page<City> page = this.cityRepository.findAll(PageRequest.of(0, 100), Optional.of(predicate), Optional.empty());
		assertNotNull(page);
		assertEquals(1, page.getContent().size());
	}

	@Test
	public void testGetEntityFilter04() {
		Predicate predicate = QCity.city.hotels.any().reviews.any().details.isNull();
		Page<City> page = this.cityRepository.findAll(PageRequest.of(0, 100), Optional.of(predicate), Optional.empty());
		assertNotNull(page);
		assertEquals(1, page.getContent().size());
		assertEquals("Melbourne", page.getContent().get(0).getName());
	}

	@Test
	public void testGetEntityFilter05() {
		Predicate predicate = QCity.city.name.upper().eq("MELBOURNE");
		Page<City> page = this.cityRepository.findAll(PageRequest.of(0, 100), Optional.of(predicate), Optional.empty());
		assertNotNull(page);
		assertEquals(2, page.getContent().size());
	}
}
