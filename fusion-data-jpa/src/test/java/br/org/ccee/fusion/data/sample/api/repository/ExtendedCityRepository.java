package br.org.ccee.fusion.data.sample.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.org.ccee.fusion.data.sample.api.model.City;

public interface ExtendedCityRepository extends CityRepository, CityRepositoryCustom {

	List<City> findByName(String name);

	@Query("SELECT city FROM City city WHERE city.country = :country")
	List<City> findByCountry(@Param("country") String country);

}
