package br.org.ccee.fusion.data.jpa.internal.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import br.org.ccee.fusion.commons.core.api.exception.NotAuthorizedException;
import br.org.ccee.fusion.data.sample.api.model.City;
import br.org.ccee.fusion.data.sample.api.repository.CityRepository;
import br.org.ccee.fusion.data.sample.internal.configuration.SampleDataJpaConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SampleDataJpaConfiguration.class)
@DataJpaTest()
public class EntityRevisionTest {

	@Autowired
	private PlatformTransactionManager platformTransactionManager;

	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private CityRepository cityRepository;

	public TransactionStatus getTransactionStatus() {
		DefaultTransactionDefinition defaultTransactionDefinition = new DefaultTransactionDefinition(
				TransactionDefinition.PROPAGATION_REQUIRES_NEW);
		return this.platformTransactionManager.getTransaction(defaultTransactionDefinition);
	}

	@Test
	public void deepUpdateTest01() throws NotAuthorizedException {

		City city = new City();
		city.setId(9L);
		city.setCountry("UK");
		city.setName("Bath (NEW)");
		city.setState("Somerset");
		city.setMap("0, 0");
		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		commitTransaction();

		beginTransaction();
		city.setName("Bath (NEW2)");
		city = this.cityRepository.save(city);
		this.cityRepository.flush();
		assertNotNull(city);
		commitTransaction();

		Map<Number, City> revisionMap = this.cityRepository.findAllRevisions(9L);
		assertNotNull(revisionMap);
		assertEquals(2, revisionMap.size());
		assertEquals("Bath (NEW)", revisionMap.get(1).getName());
		assertEquals("Bath (NEW2)", revisionMap.get(2).getName());
	}

	private void commitTransaction() {
		this.testEntityManager.getEntityManager().getTransaction().commit();
	}

	private void beginTransaction() {
		this.testEntityManager.getEntityManager().getTransaction().begin();
	}
}
