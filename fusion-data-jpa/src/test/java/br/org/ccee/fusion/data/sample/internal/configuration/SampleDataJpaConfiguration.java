package br.org.ccee.fusion.data.sample.internal.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import br.org.ccee.fusion.data.jpa.internal.configuration.DataJpaConfiguration;

@Configuration
@Import(DataJpaConfiguration.class)
@ComponentScan("br.org.ccee.fusion.data.sample")
public class SampleDataJpaConfiguration {

}
