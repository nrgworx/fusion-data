package br.org.ccee.fusion.data.sample.api.repository;

import br.org.ccee.fusion.data.sample.api.model.City;

public interface CityRepositoryCustom {

	City findFirstByCountry(String country);

}
