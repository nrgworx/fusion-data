package br.org.ccee.fusion.data.sample.api.model;

public enum TripType {

	BUSINESS, COUPLES, FAMILY, FRIENDS, SOLO

}
