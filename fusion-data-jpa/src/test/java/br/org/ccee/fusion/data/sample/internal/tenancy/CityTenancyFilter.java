package br.org.ccee.fusion.data.sample.internal.tenancy;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.commons.core.api.model.Operation;
import br.org.ccee.fusion.data.jpa.api.component.EntityFilter;
import br.org.ccee.fusion.data.sample.api.model.City;
import br.org.ccee.fusion.data.sample.api.model.QCity;

@Component
public class CityTenancyFilter implements EntityFilter<City> {

	@Override
	public Optional<Predicate> getPredicate(Operation operation) {
		return Optional.of(QCity.city.name.ne("New York"));
	}

}
