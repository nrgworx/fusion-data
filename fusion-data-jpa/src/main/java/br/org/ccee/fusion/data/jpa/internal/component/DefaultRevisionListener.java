package br.org.ccee.fusion.data.jpa.internal.component;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import br.org.ccee.fusion.data.jpa.api.model.Revision;

public class DefaultRevisionListener implements RevisionListener {

	private static final String ANONIMOUS_USERNAME = "anonymous";

	@Override
	public void newRevision(Object object) {
		Revision revision = (Revision) object;
		revision.setUsername(getUsername());
	}

	private String getUsername() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		if (securityContext != null) {
			Authentication authentication = securityContext.getAuthentication();
			if (authentication != null) {
				return authentication.getName();
			}
		}
		return ANONIMOUS_USERNAME;
	}

}
