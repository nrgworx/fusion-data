/*
 * Copyright 2000-2099 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.data.jpa.internal.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.springframework.core.GenericTypeResolver;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.commons.core.api.model.Operation;
import br.org.ccee.fusion.commons.core.internal.holder.ApplicationContextHolder;
import br.org.ccee.fusion.data.jpa.api.component.EntityFilter;

/**
 * Entity Filter Utils
 *
 * @author Thiago Assis
 */
public final class EntityFilterUtils {

	private static Map<String, EntityFilter<?>> CACHE;

	private EntityFilterUtils() {

	}

	public static Optional<Predicate> getPredicate(Class<?> entityClazz, Operation operation) {
		EntityFilter<?> entityTenancyFilter = getEntityFilter(entityClazz);
		if (entityTenancyFilter != null) {
			return entityTenancyFilter.getPredicate(operation);
		} else {
			return Optional.empty();
		}
	}

	private static EntityFilter<?> getEntityFilter(Class<?> entityClazz) {
		if (CACHE == null) {
			loadEntityTenancyFilters();
		}
		return CACHE.get(entityClazz.getName());
	}

	private static void loadEntityTenancyFilters() {
		CACHE = new HashMap<>();
		Map<String, EntityFilter> entityTenancyFilterMap = ApplicationContextHolder.getApplicationContext().getBeansOfType(EntityFilter.class);
		for (Entry<String, EntityFilter> entityTenancyFilterMapEntry : entityTenancyFilterMap.entrySet()) {
			EntityFilter<?> entityTenancyFilter = entityTenancyFilterMapEntry.getValue();
			Class<?> entityClazz = GenericTypeResolver.resolveTypeArguments(entityTenancyFilter.getClass(), EntityFilter.class)[0];
			CACHE.put(entityClazz.getName(), entityTenancyFilter);
		}
	}

}
