/*
 * Copyright 2000-2099 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.data.jpa.api.component;

import java.util.Optional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import br.org.ccee.fusion.commons.core.api.model.Operation;

/**
 * Query Factory
 *
 * @author Thiago Assis
 */
public interface QueryFactory {

	<T> JPAQuery<T> createQuery(EntityManager entityManager, Operation operation, Class<T> entityClass, Optional<Predicate> predicate, Optional<EntityGraph<T>> entityGraph);

}
