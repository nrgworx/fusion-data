package br.org.ccee.fusion.data.jpa.internal.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;

import com.querydsl.core.types.EntityPath;

public class EntityPathUtils {

	private static final EntityPathResolver entityPathResolver = SimpleEntityPathResolver.INSTANCE;

	private static Map<String, EntityPath<?>> entityPathMap = new HashMap<>();

	private EntityPathUtils() {

	}

	public static <T> EntityPath<T> createEntityPath(Class<T> clazz) {
		EntityPath<T> entityPath = (EntityPath<T>) entityPathMap.get(clazz.getName());
		if (entityPath == null) {
			entityPath = entityPathResolver.createPath(clazz);
			entityPathMap.put(clazz.getName(), entityPath);
		}
		return entityPath;
	}

}
