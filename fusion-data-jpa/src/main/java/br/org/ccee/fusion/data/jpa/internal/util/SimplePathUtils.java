package br.org.ccee.fusion.data.jpa.internal.util;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;

import com.google.common.base.CaseFormat;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.SimplePath;

public class SimplePathUtils {

	private SimplePathUtils() {

	}

	public static <T, I> SimplePath<T> createRootPath(JpaEntityInformation<T, I> jpaEntityInformation) {
		String path = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, jpaEntityInformation.getJavaType().getSimpleName());
		return Expressions.path(jpaEntityInformation.getJavaType(), path);
	}

	public static <T, I> SimplePath<I> createIdPath(JpaEntityInformation<T, I> jpaEntityInformation) {
		SimplePath<T> rootPath = createRootPath(jpaEntityInformation);
		return Expressions.path(jpaEntityInformation.getIdType(), rootPath, jpaEntityInformation.getIdAttribute().getName());
	}

}
