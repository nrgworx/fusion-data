package br.org.ccee.fusion.data.jpa.internal.util;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;

import br.org.ccee.fusion.commons.core.internal.holder.ApplicationContextHolder;

public class JpaEntityInformationUtils {

	private static final Map<String, JpaEntityInformation<?, ?>> CACHE = new HashMap<String, JpaEntityInformation<?, ?>>();

	private JpaEntityInformationUtils() {

	}

	public static <T, I> JpaEntityInformation<T, I> getJpaEntityInformation(Class<T> entityClass) {
		JpaEntityInformation<T, I> jpaEntityInformation = (JpaEntityInformation<T, I>) CACHE.get(entityClass.getName());
		if (jpaEntityInformation == null) {
			EntityManager entityManager = ApplicationContextHolder.getApplicationContext().getBean(EntityManager.class);
			jpaEntityInformation = (JpaEntityInformation<T, I>) JpaEntityInformationSupport.getEntityInformation(entityClass, entityManager);
			CACHE.put(entityClass.getName(), jpaEntityInformation);
		}
		return jpaEntityInformation;
	}

}
