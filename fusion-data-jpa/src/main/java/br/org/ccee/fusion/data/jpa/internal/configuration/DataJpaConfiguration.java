package br.org.ccee.fusion.data.jpa.internal.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.org.ccee.fusion.data.core.internal.configuration.DataCoreConfiguration;
import br.org.ccee.fusion.data.jpa.internal.component.DefaultPersistableRepository;

@Configuration
@Import({ DataCoreConfiguration.class })
@EnableConfigurationProperties
@EnableTransactionManagement
@EnableJpaRepositories(repositoryBaseClass = DefaultPersistableRepository.class, basePackages = "br.org.ccee.fusion.data.sample")
@ComponentScan("br.org.ccee.fusion.data.jpa")
public class DataJpaConfiguration {

	//private DataJpaProperties dataJpaProperties;

	public DataJpaConfiguration(DataJpaProperties dataJpaProperties) {
		//this.dataJpaProperties = dataJpaProperties;
	}

}
