package br.org.ccee.fusion.data.jpa.internal.component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.Session;
import org.hibernate.engine.spi.SelfDirtinessTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import br.org.ccee.fusion.commons.core.api.exception.NotAuthorizedException;
import br.org.ccee.fusion.commons.core.api.exception.NotFoundException;
import br.org.ccee.fusion.commons.core.api.exception.UnexpectedException;
import br.org.ccee.fusion.commons.core.api.model.Operation;
import br.org.ccee.fusion.commons.core.api.util.AssertUtils;
import br.org.ccee.fusion.commons.core.api.util.ClassUtils;
import br.org.ccee.fusion.commons.core.api.util.FieldUtils;
import br.org.ccee.fusion.commons.core.api.util.ObjectUtils;
import br.org.ccee.fusion.commons.core.api.util.OperationUtils;
import br.org.ccee.fusion.data.jpa.api.component.EntityLoader;
import br.org.ccee.fusion.data.jpa.internal.util.EntityFilterUtils;
import br.org.ccee.fusion.data.jpa.internal.util.EntityPathUtils;
import br.org.ccee.fusion.data.jpa.internal.util.EntityUtils;
import br.org.ccee.fusion.data.jpa.internal.util.JpaEntityInformationUtils;
import br.org.ccee.fusion.data.jpa.internal.util.SimplePathUtils;

@Component
@ConditionalOnSingleCandidate(EntityLoader.class)
public class DefaultEntityLoader implements EntityLoader {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEntityLoader.class);

	private final Collection<CascadeType> cascadesToSave = Arrays.asList(CascadeType.ALL, CascadeType.PERSIST);

	private final Collection<CascadeType> cascadesToDelete = Arrays.asList(CascadeType.ALL, CascadeType.REMOVE);

	@Override
	public <T, I> T load(EntityManager entityManager, Class<T> clazz, I id, Operation operation) {
		AssertUtils.parameterNotNull(entityManager, "entityManager");
		AssertUtils.parameterNotNull(id, "id");
		AssertUtils.parameterNotNull(clazz, "clazz");
		AssertUtils.parameterNotNull(operation, "operation");
		T entity = entityManager.find(clazz, id);
		return load(entityManager, entity, operation);
	}

	@Override
	public <T> T load(EntityManager entityManager, T detachedEntity, Operation operation) {
		AssertUtils.parameterNotNull(entityManager, "entityManager");
		AssertUtils.parameterNotNull(detachedEntity, "detachedEntity");
		AssertUtils.parameterNotNull(operation, "operation");
		Session session = entityManager.unwrap(Session.class);
		T attachedEntity = getAttachedEntity(session, detachedEntity, operation);
		Map<Object, Object> historyMap = new HashMap<>();
		historyMap.put(detachedEntity, attachedEntity);
		load(session, detachedEntity, attachedEntity, operation, true, historyMap);
		return attachedEntity;
	}

	private void load(Session session, Object detachedEntity, Object attachedEntity, Operation operation, boolean merge, Map<Object, Object> historyMap) throws NotAuthorizedException {

		if (merge && OperationUtils.deepCompare(Operation.SAVE, operation)) {
			merge(detachedEntity, attachedEntity);
		}

		List<Field> fields = EntityUtils.getOneToManyFields(detachedEntity);
		for (Field field : fields) {
			deepLoadMany(session, detachedEntity, attachedEntity, field, operation, mergeChild(field, operation), historyMap);
		}

		fields = EntityUtils.getOneToOneFields(detachedEntity);
		for (Field field : fields) {
			deepLoadOne(session, detachedEntity, attachedEntity, field, operation, mergeChild(field, operation), historyMap);
		}

		fields = EntityUtils.getManyToOneFields(detachedEntity);
		for (Field field : fields) {
			deepLoadOne(session, detachedEntity, attachedEntity, field, operation, mergeChild(field, operation), historyMap);
		}
	}

	private boolean mergeChild(Field field, Operation operation) {

		ManyToOne manyToOne = field.getAnnotation(ManyToOne.class);
		if (manyToOne != null) {
			return CollectionUtils.containsAny(Arrays.asList(manyToOne.cascade()), getCascades(operation));
		}

		OneToMany oneToMany = field.getAnnotation(OneToMany.class);
		if (oneToMany != null) {
			return CollectionUtils.containsAny(Arrays.asList(oneToMany.cascade()), getCascades(operation));
		}

		OneToOne oneToOne = field.getAnnotation(OneToOne.class);
		if (oneToOne != null) {
			return CollectionUtils.containsAny(Arrays.asList(oneToOne.cascade()), getCascades(operation));
		}

		throw new UnexpectedException(String.format("Unable to determine cascade for field %s from class %s", field.getName(), field.getDeclaringClass()));
	}

	private Collection<CascadeType> getCascades(Operation operation) {
		if (OperationUtils.deepCompare(Operation.SAVE, operation)) {
			return this.cascadesToSave;
		} else if (OperationUtils.deepCompare(Operation.DELETE, operation)) {
			return this.cascadesToDelete;
		}
		throw new UnexpectedException(String.format("Unable to determine cascade for operation %s", operation.toString()));
	}

	private void deepLoadMany(Session session, Object parentDetachedEntity, Object parentAttachedEntity, Field field, Operation operation, boolean merge, Map<Object, Object> historyMap) throws NotAuthorizedException {

		Object detachedFieldValue = getFieldValue(parentDetachedEntity, field);
		if (detachedFieldValue == null) {
			return;
		}

		Collection<Object> detachedChildEntities = castCollection(detachedFieldValue);
		Object attachedFieldValue = getFieldValue(parentAttachedEntity, field);
		Collection<Object> attachedChildEntities = null;
		if (attachedFieldValue != null) {
			attachedChildEntities = castCollection(attachedFieldValue);
			deleteChilds(session, detachedChildEntities, attachedChildEntities);
		} else {
			attachedChildEntities = instantiateCollection(field);
			setFieldValue(parentAttachedEntity, field, attachedChildEntities);
		}

		for (Object detachedChildEntity : detachedChildEntities) {
			Object attachedChildEntity = historyMap.get(detachedChildEntity);
			if (attachedChildEntity == null) {
				attachedChildEntity = getAttachedChildEntity(detachedChildEntity, attachedChildEntities);
				if (attachedChildEntity == null) {
					attachedChildEntity = getAttachedEntity(session, detachedChildEntity, operation);
				}
				historyMap.put(detachedChildEntity, attachedChildEntity);
				load(session, detachedChildEntity, attachedChildEntity, operation, merge, historyMap);
			}
			attachedChildEntities.add(attachedChildEntity);
		}
	}

	private void deleteChilds(Session session, Collection<Object> detachedChildEntities, Collection<Object> attachedChildEntities) {
		List<Object> attachedChildEntitiesToRemove = new ArrayList<>();
		for (Object attachedChildEntity : attachedChildEntities) {
			Object attachedChildEntityId = getIdValue(attachedChildEntity);
			Object childEntity = getEntity(attachedChildEntityId, detachedChildEntities);
			if (childEntity == null) {
				attachedChildEntitiesToRemove.add(attachedChildEntity);
				session.remove(attachedChildEntity);
				LOGGER.info(String.format("DELETE %s[%s]", attachedChildEntity.getClass().getName(), attachedChildEntityId));
			}
		}
		attachedChildEntities.removeAll(attachedChildEntitiesToRemove);
	}

	private <N> Collection<N> instantiateCollection(Field field) {
		if (List.class.isAssignableFrom(field.getType())) {
			return new ArrayList<>();
		} else if (Set.class.isAssignableFrom(field.getType())) {
			return new LinkedHashSet<>();
		} else {
			throw new UnexpectedException("Unable to instantiate OneToMany");
		}
	}

	private Object getAttachedChildEntity(Object detachedChildEntity, Collection<Object> attachedChildEntities) {
		Object attachedChildEntity = null;
		Object detachedChildEntityId = getIdValue(detachedChildEntity);
		if (detachedChildEntityId != null) {
			attachedChildEntity = getEntity(detachedChildEntityId, attachedChildEntities);
		} else {
			attachedChildEntity = ObjectUtils.newInstance(detachedChildEntity.getClass());
		}
		return attachedChildEntity;
	}

	private void deepLoadOne(Session session, Object parentDetachedEntity, Object parentAttachedEntity, Field field, Operation operation, boolean merge, Map<Object, Object> historyMap) throws NotAuthorizedException {

		Object detachedChildEntity = getFieldValue(parentDetachedEntity, field);
		if (detachedChildEntity == null) {
			return;
		}

		Object attachedChildEntity = historyMap.get(detachedChildEntity);
		if (attachedChildEntity == null) {
			attachedChildEntity = getAttachedEntity(session, detachedChildEntity, operation);
			historyMap.put(detachedChildEntity, attachedChildEntity);
			load(session, detachedChildEntity, attachedChildEntity, operation, merge, historyMap);
		}

		setFieldValue(parentAttachedEntity, field, attachedChildEntity);
	}

	private <T, I> T getAttachedEntity(Session session, T detachedEntity, Operation operation) {
		if (session.contains(detachedEntity)) {
			return detachedEntity;
		} else {
			Class<T> entityClass = ClassUtils.getClass(detachedEntity);
			I id = getIdValue(detachedEntity);
			if (id == null) {
				return ObjectUtils.newInstance(entityClass);
			} else {
				return getAttachedEntity(session, entityClass, id, operation);
			}
		}
	}

	private <T, I> T getAttachedEntity(Session session, Class<T> entityClass, I id, Operation operation) {
		LOGGER.info(String.format("LOAD %s[%s]", entityClass.getName(), id));
		EntityPath<T> entityPath = EntityPathUtils.createEntityPath(entityClass);
		JPQLQuery<T> jpqlQuery = new JPAQuery<T>(session).from(entityPath);
		JpaEntityInformation<T, I> jpaEntityInformation = JpaEntityInformationUtils.getJpaEntityInformation(entityClass);
		Predicate idPredicate = SimplePathUtils.createIdPath((JpaEntityInformation<T, I>) jpaEntityInformation).eq(id);
		Optional<Predicate> tenancyPredicate = EntityFilterUtils.getPredicate(entityClass, operation);
		if (tenancyPredicate.isPresent()) {
			jpqlQuery.where(ExpressionUtils.and(idPredicate, tenancyPredicate.get()));
		} else {
			jpqlQuery.where(idPredicate);
		}
		T attachedEntity = jpqlQuery.fetchOne();
		if (attachedEntity == null) {
			throw new NotFoundException(String.format("Entity %s[%s] not found", entityClass.getName(), id), "Check if it really exists or if you have the right permissions to access it");
		}
		return attachedEntity;
	}

	private <N> void merge(final N detachedEntity, final N attachedEntity) {

		Object detachedEntityId = null;
		if (LOGGER.isDebugEnabled()) {
			Field idField = EntityUtils.getIdField(detachedEntity.getClass());
			detachedEntityId = getFieldValue(detachedEntity, idField, true);
		}

		Collection<Field> fields = getNonRelationalFields(detachedEntity);
		for (Field field : fields) {

			if (LOGGER.isDebugEnabled()) {
				try {
					if (detachedEntityId == null) {
						detachedEntityId = "NEW";
					}
					LOGGER.debug(String.format("MERGE %s[%s].%s %s -> %s", attachedEntity.getClass().getName(), detachedEntityId, field.getName(), FieldUtils.getValue(attachedEntity, field, true), FieldUtils.getValue(detachedEntity, field, true)));
				} catch (Exception exception) {
					LOGGER.debug("Error logging merge operation", exception);
				}
			}

			try {

				Object value = FieldUtils.getValue(detachedEntity, field);

				try {
					FieldUtils.setValue(attachedEntity, field, value);
				} catch (NoSuchMethodException noSuchMethodException) {
					LOGGER.warn(noSuchMethodException.getMessage());
					continue;
				} catch (Exception exception) {
					throw new UnexpectedException(exception);
				}

			} catch (NoSuchMethodException noSuchMethodException) {
				LOGGER.warn(noSuchMethodException.getMessage());
				continue;
			} catch (Exception exception) {
				throw new UnexpectedException(exception);
			}

		}
	}

	private <N> Collection<Field> getNonRelationalFields(final N entity) {
		if (SelfDirtinessTracker.class.isAssignableFrom(entity.getClass())) {
			return getDirtyNonRelationalFields(entity);
		} else {
			return EntityUtils.getNonRelationalFields(entity);
		}
	}

	private <N> Collection<Field> getDirtyNonRelationalFields(final N entity) {
		Collection<Field> fields = new ArrayList<>();
		SelfDirtinessTracker selfDirtinessTracker = (SelfDirtinessTracker) entity;
		for (String attributeName : selfDirtinessTracker.$$_hibernate_getDirtyAttributes()) {
			Field field = FieldUtils.getField(entity, attributeName);
			if (!field.isAnnotationPresent(Id.class) && !EntityUtils.isRelationalField(field)) {
				fields.add(field);
			}
		}
		return fields;
	}

	private static <T, I> I getIdValue(T entity) {
		try {
			return EntityUtils.getIdValue(entity);
		} catch (Exception exception) {
			throw new UnexpectedException(String.format("Unable to get id of entity %s", entity.getClass().getName()), exception);
		}
	}

	private static <N, I> N getEntity(I id, Collection<N> collection) {
		try {
			return EntityUtils.getEntity(id, collection);
		} catch (Exception exception) {
			throw new UnexpectedException(String.format("Unable to get entity with id %s in collection %s", id.toString(), collection.toString()), exception);
		}
	}

	private Object getFieldValue(Object object, Field field) {
		return getFieldValue(object, field, false);
	}

	private Object getFieldValue(Object object, Field field, boolean force) {
		try {
			return FieldUtils.getValue(object, field, force);
		} catch (Exception exception) {
			throw new UnexpectedException(String.format("Unable to get %s.%s", object.getClass().getName(), field.getName()), exception);
		}
	}

	private void setFieldValue(Object object, Field field, Object value) {
		setFieldValue(object, field, value, false);
	}

	private void setFieldValue(Object object, Field field, Object value, boolean force) {
		try {
			FieldUtils.setValue(object, field, value);
		} catch (Exception exception) {
			throw new UnexpectedException(String.format("Unable to set %s.%s -> %s", object.getClass().getName(), field.getName(), value.toString()), exception);
		}
	}

	private <N> Collection<N> castCollection(Object object) {
		if (Collection.class.isAssignableFrom(object.getClass())) {
			@SuppressWarnings("unchecked")
			Collection<N> collection = (Collection<N>) object;
			return collection;
		}
		throw new UnexpectedException(String.format("%s is not assignable to %s", object.getClass().getName(), Collection.class));
	}

}
