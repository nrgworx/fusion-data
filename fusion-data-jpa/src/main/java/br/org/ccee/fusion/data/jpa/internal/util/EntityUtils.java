package br.org.ccee.fusion.data.jpa.internal.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import br.org.ccee.fusion.commons.core.api.util.AssertUtils;
import br.org.ccee.fusion.commons.core.api.util.FieldUtils;

public class EntityUtils {

	private static Map<Class<?>, Field> ID_MAP = new HashMap<>();
	private static Map<Class<?>, List<Field>> NON_RELATIONAL_FIELDS_MAP = new HashMap<>();
	private static Map<Class<?>, List<Field>> ONE_TO_MANY_FIELDS_MAP = new HashMap<>();
	private static Map<Class<?>, List<Field>> ONE_TO_ONE_FIELDS_MAP = new HashMap<>();
	private static Map<Class<?>, List<Field>> MANY_TO_ONE_FIELDS_MAP = new HashMap<>();

	private EntityUtils() {

	}

	public static <N> Field getIdField(Class<N> entityClazz) {

		AssertUtils.parameterNotNull(entityClazz, "entityClazz");

		Field idField = ID_MAP.get(entityClazz);
		if (idField != null) {
			return idField;
		}

		for (Field field : entityClazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(Id.class)) {
				idField = field;
				ID_MAP.put(entityClazz, idField);
				break;
			}
		}

		if (idField == null && entityClazz.getSuperclass() != Object.class) {
			idField = getIdField(entityClazz.getSuperclass());
		}

		return idField;
	}

	public static <T, I> I getIdValue(T entity) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		Field field = getIdField(entity.getClass());
		return (I) FieldUtils.getValue(entity, field, true);
	}

	public static <N, I> N getEntity(I id, Collection<N> collection)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		if (collection == null || collection.isEmpty()) {
			return null;
		}
		for (N entity : collection) {
			I currentId = getIdValue(entity);
			if (currentId == id) {
				return entity;
			}
		}
		return null;
	}

	public static <N> List<Field> getOneToManyFields(final N entity) {
		return getOneToManyFields(entity.getClass());
	}

	public static <N> List<Field> getOneToManyFields(Class<?> entityClazz) {
		List<Field> fields = ONE_TO_MANY_FIELDS_MAP.get(entityClazz);
		if (fields != null) {
			return fields;
		} else {
			fields = new ArrayList<>();
			ONE_TO_MANY_FIELDS_MAP.put(entityClazz, fields);
		}
		for (Field field : entityClazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(OneToMany.class) && !field.getName().equals("serialVersionUID")) {
				fields.add(field);
			}
		}
		if (entityClazz.getSuperclass() != Object.class) {
			fields.addAll(getOneToManyFields(entityClazz));
		}
		return fields;
	}

	public static <N> List<Field> getOneToOneFields(final N entity) {
		return getOneToOneFields(entity.getClass());
	}

	public static <N> List<Field> getOneToOneFields(Class<?> entityClazz) {
		List<Field> fields = ONE_TO_ONE_FIELDS_MAP.get(entityClazz);
		if (fields != null) {
			return fields;
		} else {
			fields = new ArrayList<>();
			ONE_TO_ONE_FIELDS_MAP.put(entityClazz, fields);
		}
		for (Field field : entityClazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(OneToOne.class) && !field.getName().equals("serialVersionUID")) {
				fields.add(field);
			}
		}
		if (entityClazz.getSuperclass() != Object.class) {
			fields.addAll(getOneToOneFields(entityClazz));
		}
		return fields;
	}

	public static <N> List<Field> getManyToOneFields(final N entity) {
		return getManyToOneFields(entity.getClass());
	}

	public static <N> List<Field> getManyToOneFields(Class<?> entityClazz) {
		List<Field> fields = MANY_TO_ONE_FIELDS_MAP.get(entityClazz);
		if (fields != null) {
			return fields;
		} else {
			fields = new ArrayList<>();
			MANY_TO_ONE_FIELDS_MAP.put(entityClazz, fields);
		}
		for (Field field : entityClazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(ManyToOne.class) && !field.getName().equals("serialVersionUID")) {
				fields.add(field);
			}
		}
		if (entityClazz.getSuperclass() != Object.class) {
			fields.addAll(getManyToOneFields(entityClazz));
		}
		return fields;
	}

	public static <N> List<Field> getNonRelationalFields(final N entity) {
		return getNonRelationalFields(entity.getClass());
	}

	public static <N> List<Field> getNonRelationalFields(Class<?> entityClazz) {
		List<Field> fields = NON_RELATIONAL_FIELDS_MAP.get(entityClazz);
		if (fields != null) {
			return fields;
		} else {
			fields = new ArrayList<>();
			NON_RELATIONAL_FIELDS_MAP.put(entityClazz, fields);
		}
		for (Field field : entityClazz.getDeclaredFields()) {
			if (!isRelationalField(field)) {
				fields.add(field);
			}
		}
		Class<?> superClass = entityClazz.getSuperclass();
		if (superClass != Object.class) {
			fields.addAll(getNonRelationalFields(superClass));
		}
		return fields;
	}

	public static Boolean isRelationalField(Field field) {
		if (field.isAnnotationPresent(OneToOne.class) || field.isAnnotationPresent(OneToMany.class)
				|| field.isAnnotationPresent(ManyToOne.class) || field.isAnnotationPresent(ManyToMany.class)) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

}
