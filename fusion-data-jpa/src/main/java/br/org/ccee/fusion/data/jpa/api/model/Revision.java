package br.org.ccee.fusion.data.jpa.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import br.org.ccee.fusion.data.jpa.internal.component.DefaultRevisionListener;

@Entity
@RevisionEntity(DefaultRevisionListener.class)
public class Revision extends DefaultRevisionEntity {

	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String transactionId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
