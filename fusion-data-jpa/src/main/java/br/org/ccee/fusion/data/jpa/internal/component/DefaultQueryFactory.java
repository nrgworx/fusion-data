package br.org.ccee.fusion.data.jpa.internal.component;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.AttributeNode;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Subgraph;

import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.CollectionExpression;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.SimplePath;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import br.org.ccee.fusion.commons.core.api.exception.UnexpectedException;
import br.org.ccee.fusion.commons.core.api.model.Operation;
import br.org.ccee.fusion.commons.core.api.util.FieldUtils;
import br.org.ccee.fusion.data.jpa.api.component.QueryFactory;
import br.org.ccee.fusion.data.jpa.internal.util.EntityFilterUtils;
import br.org.ccee.fusion.data.jpa.internal.util.EntityPathUtils;
import br.org.ccee.fusion.data.jpa.internal.util.JpaEntityInformationUtils;
import br.org.ccee.fusion.data.jpa.internal.util.SimplePathUtils;

@Component
@ConditionalOnSingleCandidate(QueryFactory.class)
public class DefaultQueryFactory implements QueryFactory {

	@Override
	public <T> JPAQuery<T> createQuery(EntityManager entityManager, Operation operation, Class<T> entityClass, Optional<Predicate> predicate, Optional<EntityGraph<T>> entityGraph) {
		EntityPath<T> entityPath = EntityPathUtils.createEntityPath(entityClass);
		JPAQuery<T> jpaQuery = new JPAQuery<T>(entityManager).from(entityPath);
		setPredicate(operation, entityClass, predicate, jpaQuery);
		if (entityGraph.isPresent()) {
			join(operation, entityClass, entityGraph.get().getAttributeNodes(), jpaQuery);
		}
		return jpaQuery;
	}

	private <N> void setPredicate(Operation operation, Class<?> entityClazz, Optional<Predicate> predicate, JPQLQuery<N> jpqlQuery) {
		Optional<Predicate> filterPredicate = EntityFilterUtils.getPredicate(entityClazz, operation);
		if (filterPredicate.isPresent() && predicate.isPresent()) {
			jpqlQuery.where(ExpressionUtils.and(filterPredicate.get(), predicate.get()));
		} else if (filterPredicate.isPresent() && !predicate.isPresent()) {
			jpqlQuery.where(filterPredicate.get());
		} else if (!filterPredicate.isPresent() && predicate.isPresent()) {
			jpqlQuery.where(predicate.get());
		}
	}

	private <T> void join(Operation operation, final Class<?> clazz, final List<AttributeNode<?>> attributeNodes, final JPQLQuery<T> jpqlQuery) {
		for (AttributeNode<?> attributeNode : attributeNodes) {
			if (attributeNode.getSubgraphs().isEmpty()) {
				join(operation, clazz, attributeNode.getAttributeName(), jpqlQuery);
			} else {
				JPQLQuery<T> joinnedJPQLQuery = join(operation, clazz, attributeNode.getAttributeName(), jpqlQuery);
				for (Subgraph<?> subgraph : attributeNode.getSubgraphs().values()) {
					Field field = FieldUtils.getField(clazz, attributeNode.getAttributeName());
					join(operation, getFieldType(clazz, field), subgraph.getAttributeNodes(), joinnedJPQLQuery);
				}
			}
		}
	}

	private <T, A, N> JPQLQuery<N> join(Operation operation, Class<T> clazz, String attributeName, JPQLQuery<N> jpqlQuery) {
		Field field = FieldUtils.getField(clazz, attributeName);
		if (field.isAnnotationPresent(OneToOne.class) || field.isAnnotationPresent(ManyToOne.class)) {
			return createToOneJoin(operation, clazz, field, jpqlQuery);
		} else if (field.isAnnotationPresent(OneToMany.class) || field.isAnnotationPresent(ManyToMany.class)) {
			return createToManyJoin(operation, clazz, field, jpqlQuery);
		}
		throw new RuntimeException(String.format("Field %s defined in %s not annotated with OneToOne, ManyToOne, OneToMany or ManyToMany", field.getName(), clazz));
	}

	private <T, A, N> JPQLQuery<N> createToOneJoin(Operation operation, Class<T> clazz, Field field, JPQLQuery<N> jpqlQuery) {
		Class<A> attributeClazz = getToOneFieldType(clazz, field);
		JPQLQuery<N> joinnedJPQLQuery = jpqlQuery.innerJoin(createToOneExpression(clazz, field), EntityPathUtils.createEntityPath(attributeClazz)).fetchJoin();
		setPredicate(operation, attributeClazz, Optional.empty(), joinnedJPQLQuery);
		return joinnedJPQLQuery;
	}

	private <T, A, N> JPQLQuery<N> createToManyJoin(Operation operation, Class<T> clazz, Field field, JPQLQuery<N> jpqlQuery) {
		Class<A> attributeClazz = getToManyFieldType(clazz, field);
		JPQLQuery<N> joinnedJPQLQuery = jpqlQuery.leftJoin(createToManyExpression(clazz, field), EntityPathUtils.createEntityPath(attributeClazz)).fetchJoin();
		setPredicate(operation, attributeClazz, Optional.empty(), joinnedJPQLQuery);
		return joinnedJPQLQuery;
	}

	private <T, A> EntityPath<A> createToOneExpression(Class<T> clazz, Field field) {
		JpaEntityInformation<T, ?> jpaEntityInformation = JpaEntityInformationUtils.getJpaEntityInformation(clazz);
		SimplePath<T> rootPath = SimplePathUtils.createRootPath(jpaEntityInformation);
		PathBuilder<T> pathBuilder = new PathBuilder<>(clazz, rootPath.getMetadata().getName());
		Class<A> attributeClazz = getToOneFieldType(clazz, field);
		return new PathBuilder<A>(attributeClazz, pathBuilder.getSimple(field.getName(), attributeClazz).getMetadata());
	}

	private <T, I, F> CollectionExpression<?, F> createToManyExpression(Class<T> clazz, Field field) {
		JpaEntityInformation<T, I> jpaEntityInformation = JpaEntityInformationUtils.getJpaEntityInformation(clazz);
		SimplePath<T> rootPath = SimplePathUtils.createRootPath(jpaEntityInformation);
		PathBuilder<T> pathBuilder = new PathBuilder<>(clazz, rootPath.getMetadata().getName());
		Class<F> attributeClazz = getToManyFieldType(clazz, field);
		if (field.getType().isAssignableFrom(Set.class)) {
			return pathBuilder.getSet(field.getName(), attributeClazz);
		} else if (field.getType().isAssignableFrom(List.class)) {
			return pathBuilder.getList(field.getName(), attributeClazz);
		} else if (field.getType().isAssignableFrom(Collection.class)) {
			return pathBuilder.getCollection(field.getName(), attributeClazz);
		} else {
			throw new UnexpectedException(String.format("Field %s.%s is not assignable from Set, List or Collection", clazz.getName(), field.getName()));
		}
	}

	private <T, F> Class<F> getFieldType(final Class<T> clazz, Field field) {
		if (field.isAnnotationPresent(OneToOne.class) || field.isAnnotationPresent(ManyToOne.class)) {
			return getToOneFieldType(clazz, field);
		} else if (field.isAnnotationPresent(OneToMany.class) || field.isAnnotationPresent(ManyToMany.class)) {
			return getToManyFieldType(clazz, field);
		} else {
			throw new RuntimeException(String.format("Field %s defined in %s not annotated with OneToOne, ManyToOne, OneToMany or ManyToMany", field.getName(), clazz));
		}
	}

	private <T, F> Class<F> getToOneFieldType(final Class<T> clazz, Field field) {
		@SuppressWarnings("unchecked")
		Class<F> fieldClass = (Class<F>) field.getType();
		return fieldClass;
	}

	private <T, F> Class<F> getToManyFieldType(final Class<T> clazz, Field field) {
		@SuppressWarnings("unchecked")
		Class<F> fieldClass = (Class<F>) FieldUtils.getCollectionType(field);
		return fieldClass;
	}

}
