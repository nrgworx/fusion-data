package br.org.ccee.fusion.data.jpa.internal.configuration;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "fusion.data.jpa")
public class DataJpaProperties implements InitializingBean {

	private String[] basePackages;

	private Boolean deepLoading;

	public String[] getBasePackages() {
		return basePackages;
	}

	public void setBasePackages(String[] basePackages) {
		this.basePackages = basePackages;
	}

	public Boolean getDeepLoading() {
		return deepLoading;
	}

	public void setDeepLoading(Boolean deepLoading) {
		this.deepLoading = deepLoading;
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		if (getBasePackages() == null || getBasePackages().length <= 0) {
			// TODO ver qual a exception de configuracao do spring
			//throw new 
		}

		if (getDeepLoading() == null) {
			setDeepLoading(Boolean.TRUE);
		}

	}

}
