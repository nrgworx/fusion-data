package br.org.ccee.fusion.data.jpa.api.component;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitAnyDiscriminatorColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitAnyKeyColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitBasicColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitCollectionTableNameSource;
import org.hibernate.boot.model.naming.ImplicitDiscriminatorColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitEntityNameSource;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitIdentifierColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitIndexColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitIndexNameSource;
import org.hibernate.boot.model.naming.ImplicitJoinColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitJoinTableNameSource;
import org.hibernate.boot.model.naming.ImplicitMapKeyColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitPrimaryKeyJoinColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitTenantIdColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitUniqueKeyNameSource;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;

public class DefaultImplicitNamingStrategy extends SpringImplicitNamingStrategy {

	private static final long serialVersionUID = 1L;

	@Override
	public Identifier determineJoinTableName(ImplicitJoinTableNameSource source) {
		// TODO Auto-generated method stub
		return super.determineJoinTableName(source);
	}

	@Override
	public Identifier determinePrimaryTableName(ImplicitEntityNameSource source) {
		// TODO Auto-generated method stub
		return super.determinePrimaryTableName(source);
	}

	@Override
	public Identifier determineCollectionTableName(ImplicitCollectionTableNameSource source) {
		// TODO Auto-generated method stub
		return super.determineCollectionTableName(source);
	}

	@Override
	public Identifier determineIdentifierColumnName(ImplicitIdentifierColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineIdentifierColumnName(source);
	}

	@Override
	public Identifier determineDiscriminatorColumnName(ImplicitDiscriminatorColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineDiscriminatorColumnName(source);
	}

	@Override
	public Identifier determineTenantIdColumnName(ImplicitTenantIdColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineTenantIdColumnName(source);
	}

	@Override
	public Identifier determineBasicColumnName(ImplicitBasicColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineBasicColumnName(source);
	}

	@Override
	public Identifier determineJoinColumnName(ImplicitJoinColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineJoinColumnName(source);
	}

	@Override
	public Identifier determinePrimaryKeyJoinColumnName(ImplicitPrimaryKeyJoinColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determinePrimaryKeyJoinColumnName(source);
	}

	@Override
	public Identifier determineAnyDiscriminatorColumnName(ImplicitAnyDiscriminatorColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineAnyDiscriminatorColumnName(source);
	}

	@Override
	public Identifier determineAnyKeyColumnName(ImplicitAnyKeyColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineAnyKeyColumnName(source);
	}

	@Override
	public Identifier determineMapKeyColumnName(ImplicitMapKeyColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineMapKeyColumnName(source);
	}

	@Override
	public Identifier determineListIndexColumnName(ImplicitIndexColumnNameSource source) {
		// TODO Auto-generated method stub
		return super.determineListIndexColumnName(source);
	}

	@Override
	public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {
		// TODO Auto-generated method stub
		return super.determineForeignKeyName(source);
	}

	@Override
	public Identifier determineUniqueKeyName(ImplicitUniqueKeyNameSource source) {
		// TODO Auto-generated method stub
		return super.determineUniqueKeyName(source);
	}

	@Override
	public Identifier determineIndexName(ImplicitIndexNameSource source) {
		// TODO Auto-generated method stub
		return super.determineIndexName(source);
	}

}
