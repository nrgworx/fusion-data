/*
 * Copyright 2000-2099 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.data.jpa.internal.component;

import java.util.List;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.Subgraph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.data.jpa.repository.query.JpaEntityMetadata;
import org.springframework.stereotype.Component;

import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.commons.core.api.model.ObjectGraphNode;
import br.org.ccee.fusion.commons.core.api.util.AssertUtils;
import br.org.ccee.fusion.data.jpa.api.component.EntityGraphParser;

/**
 * Default Entity Graph Parser
 *
 * @author Thiago Assis
 */
@Component
@ConditionalOnSingleCandidate(EntityGraphParser.class)
public class DefaultEntityGraphParser implements EntityGraphParser {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEntityGraphParser.class);

	@Override
	public <N> EntityGraph<N> parse(final EntityManager entityManager, final JpaEntityMetadata<N> jpaEntityMetadata, final ObjectGraph objectGraph) {
		AssertUtils.parameterNotNull(entityManager, "entityManager");
		AssertUtils.parameterNotNull(jpaEntityMetadata, "JpaEntityMetadata");
		AssertUtils.parameterNotNull(objectGraph, "objectGraph");
		EntityGraph<N> entityGraph = entityManager.createEntityGraph(jpaEntityMetadata.getJavaType());
		for (ObjectGraphNode objectGraphNode : objectGraph.getNodes()) {
			List<ObjectGraphNode> childrenNodes = objectGraphNode.getChildren();
			if (childrenNodes.isEmpty()) {
				entityGraph.addAttributeNodes(objectGraphNode.getName());
			} else {
				addSubgraph(entityGraph.addSubgraph(objectGraphNode.getName()), childrenNodes);
			}
		}
		LOGGER.info(String.format("EntityGraph %s generated for entity %s", entityGraph, jpaEntityMetadata.getEntityName()));
		return entityGraph;
	}

	private <N> void addSubgraph(Subgraph<N> subgraph, List<ObjectGraphNode> objectGraphNodes) {
		for (ObjectGraphNode objectGraphNode : objectGraphNodes) {
			List<ObjectGraphNode> childrenNodes = objectGraphNode.getChildren();
			if (childrenNodes.isEmpty()) {
				subgraph.addAttributeNodes(objectGraphNode.getName());
			} else {
				addSubgraph(subgraph.addSubgraph(objectGraphNode.getName()), childrenNodes);
			}
		}
	}

}
