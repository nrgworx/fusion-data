package br.org.ccee.fusion.data.jpa.internal.component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Persistable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaPersistableEntityInformation;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.support.PageableExecutionUtils;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Ops;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.SimplePath;
import com.querydsl.jpa.JPQLQuery;

import br.org.ccee.fusion.commons.core.api.exception.NotFoundException;
import br.org.ccee.fusion.commons.core.api.exception.NotSupportedException;
import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.commons.core.api.model.Operation;
import br.org.ccee.fusion.commons.core.api.util.AssertUtils;
import br.org.ccee.fusion.commons.core.internal.holder.ApplicationContextHolder;
import br.org.ccee.fusion.data.core.api.repository.PersistableRepository;
import br.org.ccee.fusion.data.jpa.api.component.EntityGraphParser;
import br.org.ccee.fusion.data.jpa.api.component.EntityLoader;
import br.org.ccee.fusion.data.jpa.api.component.QueryFactory;
import br.org.ccee.fusion.data.jpa.internal.configuration.DataJpaProperties;
import br.org.ccee.fusion.data.jpa.internal.util.EntityPathUtils;
import br.org.ccee.fusion.data.jpa.internal.util.SimplePathUtils;

// https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#core.web.type-safe
// https://stackoverflow.com/questions/42133023/is-there-a-way-to-register-a-repository-base-class-with-a-spring-boot-auto-confi
public class DefaultPersistableRepository<T extends Persistable<I>, I extends Serializable> extends SimpleJpaRepository<T, I> implements PersistableRepository<T, I> {

	//private static final Logger LOGGER = LoggerFactory.getLogger(HibernatePersistableRepository.class);

	private static final String SPECIFICATION_NOT_SUPPORTED_MESSAGE = "Specifications is not supported use Querydsl predicate instead";
	private static final String EXAMPLE_NOT_SUPPORTED_MESSAGE = "Example is not supported use Querydsl predicate instead";

	private DataJpaProperties dataJpaProperties;

	private final JpaPersistableEntityInformation<T, I> jpaPersistableEntityInformation;

	private final EntityManager entityManager;

	private EntityPath<T> entityPath;

	private final Querydsl querydsl;

	private final SimplePath<I> idPath;

	private EntityLoader entityLoader;

	private QueryFactory queryFactory;

	private EntityGraphParser entityGraphParser;

	public DefaultPersistableRepository(JpaPersistableEntityInformation<T, I> jpaPersistableEntityInformation, EntityManager entityManager) {
		super(jpaPersistableEntityInformation, entityManager);
		this.jpaPersistableEntityInformation = jpaPersistableEntityInformation;
		this.entityManager = entityManager;
		this.entityPath = EntityPathUtils.createEntityPath(jpaPersistableEntityInformation.getJavaType());
		this.querydsl = new Querydsl(entityManager,new PathBuilder<>(this.entityPath.getType(), this.entityPath.getMetadata()));
		this.idPath = SimplePathUtils.createIdPath(jpaPersistableEntityInformation);
	}

	protected DataJpaProperties getDataJpaProperties() {
		if (this.dataJpaProperties == null) {
			this.dataJpaProperties = ApplicationContextHolder.getApplicationContext().getBean(DataJpaProperties.class);
		}
		return this.dataJpaProperties;
	}

	protected EntityLoader getEntityLoader() {
		if (this.entityLoader == null) {
			this.entityLoader = ApplicationContextHolder.getApplicationContext().getBean(EntityLoader.class);
		}
		return this.entityLoader;
	}

	protected QueryFactory getQueryFactory() {
		if (this.queryFactory == null) {
			this.queryFactory = ApplicationContextHolder.getApplicationContext().getBean(QueryFactory.class);
		}
		return this.queryFactory;
	}

	protected EntityGraphParser getEntityGraphParser() {
		if (this.entityGraphParser == null) {
			this.entityGraphParser = ApplicationContextHolder.getApplicationContext().getBean(EntityGraphParser.class);
		}
		return this.entityGraphParser;
	}

	@Override
	public Stream<T> saveAll(Stream<T> entities) {
		AssertUtils.parameterNotNull(entities, "entities");
		return entities.map(entity -> save(entity));
	}

	@Override
	public T save(T entity) {
		AssertUtils.parameterNotNull(entity, "entity");
		DataJpaProperties dataJpaProperties = this.getDataJpaProperties();
		if (dataJpaProperties.getDeepLoading()) {
			T loadedEntity =  getEntityLoader().load(this.entityManager, entity, Operation.SAVE);
			return this.createOrUpdate(loadedEntity);
		} else {
			return this.createOrUpdate(entity);
		}
	}

	@Override
	public Stream<T> findAll(Optional<Predicate> predicate, Optional<ObjectGraph> objectGraph) {
		AssertUtils.parameterNotNull(predicate, "predicate");
		AssertUtils.parameterNotNull(objectGraph, "objectGraph");
		Optional<EntityGraph<T>> entityGraph = parseEntityGraph(objectGraph);
		return fetchStream(Operation.RETRIEVE, predicate, entityGraph);
	}

	@Override
	public Page<T> findAll(Pageable pageable, Optional<Predicate> predicate, Optional<ObjectGraph> objectGraph) {
		AssertUtils.parameterNotNull(pageable, "pageable");
		AssertUtils.parameterNotNull(predicate, "predicate");
		AssertUtils.parameterNotNull(objectGraph, "objectGraph");
		Optional<EntityGraph<T>> entityGraph = parseEntityGraph(objectGraph);
		return this.fetchPage(Operation.RETRIEVE, pageable, predicate, entityGraph);
	}

	@Override
	public Optional<T> findById(I id, Optional<ObjectGraph> objectGraph) {
		AssertUtils.parameterNotNull(id, "id");
		AssertUtils.parameterNotNull(objectGraph, "objectGraph");
		Predicate idPredicate = Expressions.predicate(Ops.EQ, this.idPath, Expressions.constant(id));
		Optional<EntityGraph<T>> entityGraph = parseEntityGraph(objectGraph);
		return this.fetchOne(Operation.RETRIEVE, Optional.of(idPredicate), entityGraph);
	}

	@Override
	public Map<Number, T> findAllRevisions(I id) {
		AssertUtils.parameterNotNull(id, "id");
		AuditReader auditReader = AuditReaderFactory.get(this.entityManager);
		Map<Number, T> revisionsMap = new LinkedHashMap<>();
		List<Number> revisions = auditReader.getRevisions(this.jpaPersistableEntityInformation.getJavaType(), id);
		for (Number revisionNumber : revisions) {
			T entity = auditReader.find(this.jpaPersistableEntityInformation.getJavaType(), id, revisionNumber);
			revisionsMap.put(revisionNumber, entity);
		}
		return revisionsMap;
	}

	@Override
	public void deleteAll(Predicate predicate) {
		AssertUtils.parameterNotNull(predicate, "predicate");
		this.fetchStream(Operation.DELETE, Optional.of(predicate), Optional.empty())
			.map(entity -> {
				delete((T) entity);
				return entity;
			});
	}

	@Override
	public void deleteAll(final Stream<T> entities) {
		AssertUtils.parameterNotNull(entities, "entities");
		entities.map(entity -> {
			delete(entity);
			return entity;
		});
	}

	@Override
	public void deleteById(I id) {
		AssertUtils.parameterNotNull(id, "id");
		DataJpaProperties dataJpaProperties = this.getDataJpaProperties();
		if (dataJpaProperties.getDeepLoading()) {
			T loadedEntity = getEntityLoader().load(this.entityManager, this.jpaPersistableEntityInformation.getJavaType(), id, Operation.DELETE);
			this.remove(loadedEntity);
		} else {
			this.remove(id);
		}
	}

	@Override
	public void delete(final T entity) {
		AssertUtils.parameterNotNull(entity, "entity");
		DataJpaProperties dataJpaProperties = this.getDataJpaProperties();
		if (dataJpaProperties.getDeepLoading()) {
			T loadedEntity = getEntityLoader().load(this.entityManager, entity, Operation.DELETE);
			this.remove(loadedEntity);
		} else {
			this.remove(entity);
		}
	}

	@Override
	public void flush() {
		getSession().flush();
	}

	private T createOrUpdate(T entity) {
		if (this.jpaPersistableEntityInformation.isNew(entity)) {
			getSession().save(entity);
		} else {
			getSession().update(entity);
		}
		return entity;
	}

	private Optional<EntityGraph<T>> parseEntityGraph(Optional<ObjectGraph> objectGraph) {
		if (objectGraph.isPresent()) {
			return Optional.of(getEntityGraphParser().parse(this.entityManager, this.jpaPersistableEntityInformation, objectGraph.get()));
		} else {
			return Optional.empty();
		}
	}

	private Stream<T> fetchStream(Operation operation, Optional<Predicate> predicate, Optional<EntityGraph<T>> entityGraph) {
		Query query = this.getQueryFactory().createQuery(this.entityManager, operation, this.jpaPersistableEntityInformation.getJavaType(), predicate, entityGraph).createQuery();
		@SuppressWarnings("unchecked")
		Stream<T> stream = (Stream<T>) query.getResultStream();
		return stream;
	}

	private Page<T> fetchPage(Operation operation, Pageable pageable, Optional<Predicate> predicate, Optional<EntityGraph<T>> entityGraph) {
		JPQLQuery<T> mainQuery = this.getQueryFactory().createQuery(this.entityManager, operation, this.jpaPersistableEntityInformation.getJavaType(), predicate, entityGraph);
		JPQLQuery<T> countQuery = this.getQueryFactory().createQuery(this.entityManager, operation, this.jpaPersistableEntityInformation.getJavaType(), predicate, Optional.empty());
		this.querydsl.applyPagination(pageable, mainQuery);
		return PageableExecutionUtils.getPage(mainQuery.fetch(), pageable, countQuery::fetchCount);
	}

	private Optional<T> fetchOne(Operation operation, Optional<Predicate> predicate, Optional<EntityGraph<T>> entityGraph) {
		return Optional.ofNullable(this.getQueryFactory().createQuery(this.entityManager, operation, this.jpaPersistableEntityInformation.getJavaType(), predicate, entityGraph).fetchOne());
	}

	private void remove(I id) {
		getSession().delete(id);
	}

	private void remove(T entity) {
		getSession().delete(entity);
	}

	private Session getSession() {
		return this.entityManager.unwrap(Session.class);
	}

	@Override
	public void deleteAll(Iterable<? extends T> entities) {
		entities.forEach(entity -> this.delete(entity));
	}

	@Override
	public void deleteInBatch(Iterable<T> entities) {
		// TODO Auto-generated method stub
		super.deleteInBatch(entities);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		super.deleteAll();
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		super.deleteAllInBatch();
	}

	@Override
	public Optional<T> findById(I id) {
		return this.findById(id, Optional.empty());
	}

	@Override
	public T getOne(I id) {
		return this.findById(id, Optional.empty())
				.orElseThrow(() -> new NotFoundException(String.format("Entity id %s not found", id)));
	}

	@Override
	public boolean existsById(I id) {
		// TODO Auto-generated method stub
		return super.existsById(id);
	}

	@Override
	public List<T> findAll() {
		return this.findAll(Optional.empty(), Optional.empty())
				.collect(Collectors.toList());
	}

	@Override
	public List<T> findAllById(Iterable<I> ids) {
		List<T> entities = new ArrayList<T>();
		ids.forEach(id -> entities.add(this.getOne(id)));
		return entities;
	}

	@Override
	public List<T> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return this.findAll(sort);
	}

	@Override
	public Page<T> findAll(Pageable pageable) {
		return this.findAll(pageable, Optional.empty(), Optional.empty());
	}

	@Override
	public Optional<T> findOne(Specification<T> spec) {
		throw new NotSupportedException(SPECIFICATION_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public List<T> findAll(Specification<T> spec) {
		throw new NotSupportedException(SPECIFICATION_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public Page<T> findAll(Specification<T> spec, Pageable pageable) {
		throw new NotSupportedException(SPECIFICATION_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public List<T> findAll(Specification<T> spec, Sort sort) {
		throw new NotSupportedException(SPECIFICATION_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public <S extends T> Optional<S> findOne(Example<S> example) {
		throw new NotSupportedException(EXAMPLE_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public <S extends T> long count(Example<S> example) {
		throw new NotSupportedException(EXAMPLE_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public <S extends T> boolean exists(Example<S> example) {
		throw new NotSupportedException(EXAMPLE_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public <S extends T> List<S> findAll(Example<S> example) {
		throw new NotSupportedException(EXAMPLE_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public <S extends T> List<S> findAll(Example<S> example, Sort sort) {
		throw new NotSupportedException(EXAMPLE_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public <S extends T> Page<S> findAll(Example<S> example, Pageable pageable) {
		throw new NotSupportedException(EXAMPLE_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return super.count();
	}

	@Override
	public long count(Specification<T> spec) {
		throw new NotSupportedException(SPECIFICATION_NOT_SUPPORTED_MESSAGE);
	}

	@Override
	public <S extends T> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return super.save(entity);
	}

	@Override
	public <S extends T> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return super.saveAll(entities);
	}

}
