@QueryEntities({ DefaultRevisionEntity.class })
package br.org.ccee.fusion.data.jpa.api.model;

import org.hibernate.envers.DefaultRevisionEntity;

import com.querydsl.core.annotations.QueryEntities;
