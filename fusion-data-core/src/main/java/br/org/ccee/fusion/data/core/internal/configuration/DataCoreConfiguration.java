package br.org.ccee.fusion.data.core.internal.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import br.org.ccee.fusion.commons.core.internal.configuration.CommonsCoreConfiguration;

@Configuration
@EnableConfigurationProperties
@Import({ CommonsCoreConfiguration.class })
@ComponentScan("br.org.ccee.fusion.data.core")
public class DataCoreConfiguration {

	// private DataCoreProperties dataCoreProperties;

	public DataCoreConfiguration(DataCoreProperties dataCoreProperties) {
		// this.dataCoreProperties = dataCoreProperties;
	}

}
