package br.org.ccee.fusion.data.core.api.service;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Persistable;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.data.core.api.repository.PersistableRepository;

public class AbstractPersistableService<P extends Persistable<ID>, ID extends Serializable> implements PersistableService<P, ID> {

	private final PersistableRepository<P, ID> persistableRepository;

	public AbstractPersistableService(PersistableRepository<P, ID> persistableRepository) {
		this.persistableRepository = persistableRepository;
	}

	@Override
	public Stream<P> createAll(Stream<P> persistables) {
		return this.persistableRepository.saveAll(persistables);
	}

	@Override
	public P create(P persistable) {
		return this.persistableRepository.save(persistable);
	}

	@Override
	public Stream<P> findAll(Optional<Predicate> predicate, Optional<ObjectGraph> objectGraph) {
		return this.persistableRepository.findAll(predicate, objectGraph);
	}

	@Override
	public Page<P> findAll(Pageable pageable, Optional<Predicate> predicate, Optional<ObjectGraph> objectGraph) {
		return this.persistableRepository.findAll(pageable, predicate, objectGraph);
	}

	@Override
	public Optional<P> findById(ID id, Optional<ObjectGraph> objectGraph) {
		return this.persistableRepository.findById(id, objectGraph);
	}

	@Override
	public Map<Number, P> findAllRevisions(ID id) {
		return this.persistableRepository.findAllRevisions(id);
	}

	@Override
	public Stream<P> updateAll(Stream<P> persistables) {
		return this.persistableRepository.saveAll(persistables);
	}

	@Override
	public P update(P persistable) {
		return this.persistableRepository.save(persistable);
	}

	@Override
	public void deleteAll(Stream<P> persistables) {
		this.persistableRepository.deleteAll(persistables);
	}

	@Override
	public void delete(ID id) {
		this.persistableRepository.deleteById(id);
	}

	@Override
	public void delete(P persistable) {
		this.persistableRepository.delete(persistable);
	}

}
