package br.org.ccee.fusion.data.core.api.service;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Persistable;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;
import br.org.ccee.fusion.commons.core.api.service.GenericService;

/**
 * Persistable service
 *
 * @author Thiago Assis
 */
public interface PersistableService<P extends Persistable<ID>, ID extends Serializable> extends GenericService<P> {

	Stream<P> createAll(Stream<P> persistables);

	P create(P persistable);

	Stream<P> findAll(Optional<Predicate> predicate, Optional<ObjectGraph> objectGraph);

	Page<P> findAll(Pageable pageable, Optional<Predicate> predicate, Optional<ObjectGraph> objectGraph);

	Optional<P> findById(ID id, Optional<ObjectGraph> objectGraph);

	Map<Number, P> findAllRevisions(ID id);

	Stream<P> updateAll(Stream<P> persistables);

	P update(P persistable);

	void deleteAll(Stream<P> persistables);

	void delete(ID id);

	void delete(P persistable);

}
