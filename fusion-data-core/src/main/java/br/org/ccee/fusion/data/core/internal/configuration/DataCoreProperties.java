package br.org.ccee.fusion.data.core.internal.configuration;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "fusion.data.core")
public class DataCoreProperties implements InitializingBean {

	@Override
	public void afterPropertiesSet() throws Exception {

	}

}
