package br.org.ccee.fusion.data.core.api.repository;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface GenericRepository<T, ID extends Serializable> extends Repository<T, ID> {

}
