package br.org.ccee.fusion.data.core.api.repository;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Persistable;
import org.springframework.data.repository.NoRepositoryBean;

import com.querydsl.core.types.Predicate;

import br.org.ccee.fusion.commons.core.api.model.ObjectGraph;

/**
 * Persistable repository interface.
 *
 * @author Thiago Assis
 */
@NoRepositoryBean
public interface PersistableRepository<P extends Persistable<ID>, ID extends Serializable> extends GenericRepository<P, ID> {

	Stream<P> saveAll(Stream<P> persistables);

	P save(P persistable);

	Stream<P> findAll(Optional<Predicate> predicate, Optional<ObjectGraph> objectGraph);

	Page<P> findAll(Pageable pageable, Optional<Predicate> predicate, Optional<ObjectGraph> objectGraph);

	Optional<P> findById(ID id, Optional<ObjectGraph> objectGraph);

	Map<Number, P> findAllRevisions(ID id);

	void deleteAll(Predicate predicate);

	void deleteAll(Stream<P> persistables);

	void deleteById(ID id);

	void delete(P persistable);

	void flush();

}
